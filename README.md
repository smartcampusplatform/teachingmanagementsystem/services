1. File html hasilKuesionerDosen akan menampilkan hasil kuesioner dosen berdasarkan input yang sudah diberikan oleh mahasiswa
2. File html hasilKuesionerPerkuliahan akan menampilkan hasil kuesioner perkuliahan berdasarkan input yang sudah diberikan oleh mahasiswa terkait mata kuliah berdasarkan silabus mata kuliah tersebut
3. File isiKuesioner.html merupakan laman untuk mengisi evaluasi perkuliahan yang hanya bisa diisi oleh mahasiswa
4. File isiLihatEvalDosen.html merupakan laman untuk dosen / kaprodi / dekan menginput data agar mereka bisa melihat hasil evaluasi dosen 
5. File isiLihatEvalPerkuliahan.html merupakan laman untuk dosen / kaprodi / dekan menginput data agar mereka bisa melihat hasil evaluasi perkuliahan
6. File KuesionerDosen.html merupakan laman untuk mengisi evaluasi perkuliahan yang hanya bisa diisi oleh mahasiswa
7. File LihatKuesioner.html merupakan laman untuk melihat kuesioner mana yang harus diisi oleh mahasiswa terkait mata kuliah maupun dosen pengajar